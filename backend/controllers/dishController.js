import asyncHandler from 'express-async-handler';
import Dish from '../models/dishModel.js';

/**
 * @desc		Get all dishes
 * @route		GET /api/
 * @access	public
 */
const getAllDishes = asyncHandler(async (req, res) => {
    const dishes = await Dish.find({});
    res.json(dishes);
});


/**
 * @desc		Get category
 * @route		GET /api/menu
 * @access	public
 */
const getCategory = asyncHandler(async (req, res) => {
    
    const filteredDishes = await Dish.find({category: req.params.category});
    if (filteredDishes){
        res.json(filteredDishes);
    } else {
        res.status(404).json({ message: 'Category not found' });
    }

});


/**
 * @desc		Get single dish
 * @route		GET /api/menu/dish/:id
 * @access	public
 */
const getDishById = asyncHandler(async (req, res) => {
		
    const dish = await Dish.findById(req.params.id);
    if (dish) {
        res.json(dish);
    } else {
        res.status(404).json({ message: 'Dish not found' });
    }

});


export { getAllDishes, getCategory, getDishById};