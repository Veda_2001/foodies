import colors from 'colors';
import dotenv from 'dotenv';
import connectDB from './config/db.js';
import dishes from './data/dishes.js';
import users from './data/users.js'
import Dish from './models/dishModel.js';
import User from './models/userModel.js';
import Order from './models/orderModel.js';


dotenv.config();

connectDB();

const importData = async () => {
	try {
		await Dish.deleteMany();
		await User.deleteMany();
        await Order.deleteMany();


		const createdUsers = await User.insertMany(users);
		const adminUser = createdUsers[0]._id;

		const sampleDishes = dishes.map((dish) => {
			return { ...dish, user: adminUser };
		});
		await Dish.insertMany(sampleDishes);

		console.log('Data imported'.green.inverse);
		process.exit();
	} catch (err) {
		console.error(`${err}`.red.inverse);
		process.exit(1);
	}
};

const destroyData = async () => {
	try {
		await Dish.deleteMany();
		await User.deleteMany();
        await Order.deleteMany();

		console.log('Data destroyed'.red.inverse);
		process.exit();
	} catch (err) {
		console.error(`${err}`.red.inverse);
		process.exit(1);
	}
};

if (process.argv[2] === '-d') {
	destroyData();
} else {
	importData();
}
