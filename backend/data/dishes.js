
const dishes = [
    {
        
        name: 'Aglio e Olio',
        category: 'Italian',
        image:'/images/aglio_e_olio.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 250,
    },
    {
        
        name: 'Fettuccine Alfredo',
        category: 'Italian',
        image:'/images/alferdo_pasta.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.7,
        price: 300,
    },
    {
        
        name: 'Aloo Paratha',
        category: 'Indian',
        image:'/images/aloo_paratha.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 80,
    },
    {
        
        name: 'Basil Pizza',
        category: 'Italian',
        image:'/images/basil_pizza.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.1,
        price: 290,
    },
    {
        
        name: 'Bibimbap',
        category: 'Korean',
        image:'/images/bibimbap.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 450,
    },
    {
        
        name: 'Bomb Cake',
        category: 'Dessert',
        image:'/images/bomb_cake.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 200,
    },
    {
        
        name: 'Bread',
        category: 'Italian',
        image:'/images/bread.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.4,
        price: 150,
    },
    {
        
        name: 'Burger and Fries',
        category: 'Italian',
        image:'/images/burger_and_fries.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 3.9,
        price: 170,
    },
    {
       
        name: 'Butter Aloo Paratha',
        category: 'Indian',
        image:'/images/butter_aloo_paratha.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.7,
        price: 200,
    },
    {

        
        name: 'Cake Shot',
        category: 'Dessert',
        image:'/images/cake_shot.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 150,
    },
    {

        
        name: 'Caramel Ice Cream',
        category: 'Dessert',
        image:'/images/caramel_icecream.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 220,
    },
    {

        
        name: 'Caramel Pudding',
        category: 'Dessert',
        image:'/images/caramel_pudding.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 35,
    },
    {

        
        name: 'Cauliflower Chilly',
        category: 'Indo-Chinese',
        image:'/images/cauliflower_dry.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 180,
    },
    {

        
        name: 'Veggies Chesse Pizza',
        category: 'Italian',
        image:'/images/cheese_pizza.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.0,
        price: 210,
    },
    {

        
        name: 'Chesse Red Sauce Pasta',
        category: 'Italian',
        image:'/images/chesse_red_pasta.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 310,
    },
    {

        
        name: 'Chilly Potato',
        category: 'Indo-Chinese',
        image:'/images/chilli_potato.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 190,
    },
    {

        
        name: 'Choco Lava Cake',
        category: 'Dessert',
        image:'/images/choco_lava_cake.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.9,
        price: 440,
    },
    {

        
        name: 'Churros',
        category: 'Dessert',
        image:'/images/churros.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 200,
    },
    {

        
        name: 'Lo Mein',
        category: 'Chinese',
        image:'/images/chinese_noodles.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.7,
        price: 390,
    },
    {

        
        name: 'Clamp Pot',
        category: 'Sea Food',
        image:'/images/clamp.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 320,
    },
    {
        
       name: 'Clamp Pasta',
        category: 'Italian',
        image:'/images/clamp_pasta.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.7,
        price: 420,
    },
    {
        
       name: 'Burger, Fries and Mini-Pizza Combo',
        category: 'Italian',
        image:'/images/combo.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.1,
        price: 290,
    },
    {
        
        name: 'Corn Dog',
        category: 'Korean',
        image:'/images/corn_dog.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.9,
        price: 200,
    },
    {

        
        name: 'Crab Curry',
        category: 'Sea Food',
        image:'/images/crab.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 500,
    },
    {

        
        name: 'Dahi Vada',
        category: 'Indian',
        image:'/images/dahi_vada.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 3.9,
        price: 120,
    },
    {

        
        name: 'Dhokla',
        category: 'Indian',
        image:'/images/dhokla.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 70,
    },
    {

        
        name: 'Masala Dosa',
        category: 'Indian',
        image:'/images/dosa.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.7,
        price: 80,
    },
    {

        
        name: 'Dumpling',
        category: 'Chinese',
        image:'/images/dumpling.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 300,
    },
    {

        
        name: 'Farm Pizza',
        category: 'Italian',
        image:'/images/farm_pizza.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 275,
    },
    {

        
        name: 'Frankie',
        category: 'Indian',
        image:'/images/frankie.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.4,
        price: 40,
    },
    {

        
        name: 'Fried Milk',
        category: 'Dessert',
        image:'/images/fried_milk.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 200,
    },
    {

        
        name: 'Choco Fudge with Ice Cream',
        category: 'Dessert',
        image:'/images/fudge.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 130,
    },
    {

        
        name: 'Gralic Bread',
        category: 'Italian',
        image:'/images/garlic_bread.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 200,
    },
    {

        
        name: 'Garlic Naan',
        category: 'Indian',
        image:'/images/garlic_naan.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 85,
    },
    {

        
        name: 'Garlic Parsley Pasta',
        category: 'Italian',
        image:'/images/garlic_parsley_pasta.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 360,
    },
    {

        
        name: 'Garlic Sesame Noodles',
        category: 'Korean',
        image:'/images/garlic_sesame_noodles.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 390,
    },
    {

        
        name: 'Gimbap',
        category: 'Korean',
        image:'/images/gimbap.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 200,
    },
    {

        
        name: 'Gujrati Thali',
        category: 'Indian',
        image:'/images/gujrati_thali.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 280,
    },
    {

        
        name: 'Ice Cream Cake',
        category: 'Dessert',
        image:'/images/icecream_cake.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 90,
    },
    {

        
        name: 'Idli',
        category: 'Indian',
        image:'/images/idli.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 60,
    },
    {

        
        name: 'Jajangmyeon',
        category: 'Korean',
        image:'/images/jajangmeyon.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 480,
    },
    {

        
        name: 'Japense Curry',
        category: 'Japanese',
        image:'/images/jap_curry.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.7,
        price: 500,
    },
    {

        
        name: 'Mutter Puloa and Kadai Panner',
        category: 'Indian',
        image:'/images/kadai_pan_puloa_combo.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 200,
    },
    {

        
        name: 'Kimchi',
        category: 'Korean',
        image:'/images/kimchi.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 430,
    },
    {

        
        name: 'Kimchi Noodles',
        category: 'Korean',
        image:'/images/kimchi_noodles.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 450,
    },
    {

        
        name: 'Lobster',
        category: 'Sea Food',
        image:'/images/lobster.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.9,
        price: 350,
    },
    {

        
        name: 'Macroon',
        category: 'Dessert',
        image:'/images/macrooon.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 180,
    },
    {

        
        name: 'Maharashtrian Thali',
        category: 'Indian',
        image:'/images/maharashtrian_thali.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 300,
    },
    {

        
        name: 'Manchurian',
        category: 'Indo-Chinese',
        image:'/images/manchurian.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 200,
    },
    {

        
        name: 'Medu Vada',
        category: 'Indian',
        image:'/images/medu_vada.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 60,
    },
    {

        
        name: 'Mini Pizza',
        category: 'Italian',
        image:'/images/mini_pizza.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 290,
    },
    {

        
        name: 'Misal Pav',
        category: 'Indian',
        image:'/images/misal_pav.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 90,
    },

    {

        
        name: 'Mochi',
        category: 'Japanese',
        image:'/images/mochi.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.9,
        price: 340,
    },
    {

        
        name: 'Tandoori Fried Momos',
        category: 'Indo-Chinese',
        image:'/images/momo.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 100,
    },
    {

        
        name: 'Mushroom Noodles',
        category: 'Chinese',
        image:'/images/mush_noodles.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 350,
    },
    {

        
        name: 'Naan',
        category: 'Indian',
        image:'/images/naan.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 50,
    },
    {

        
        name: 'North Indian Small Thali',
        category: 'Indian',
        image:'/images/north_indian_combo.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 290,
    },
    {

        
        name: 'North Indian Thali',
        category: 'Indian',
        image:'/images/north_indian_thali.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 280,
    },
    {

        
        name: 'Okonomiyaki',
        category: 'Japanese',
        image:'/images/okonomiyaki.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 450,
    },
    {

        
        name: 'Omu Rice',
        category: 'Japanese',
        image:'/images/omu_rice.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.7,
        price: 430,
    },
    {

        
        name: 'Onigri',
        category: 'Japanese',
        image:'/images/onigiri.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 290,
    },
    {

        
        name: 'Palak Paratha',
        category: 'Indian',
        image:'/images/palak_paratha.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 90,
    },
    {

        
        name: 'Panner Paratha',
        category: 'Indian',
        image:'/images/panner_paratha.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.7,
        price: 100,
    },
    {

        
        name: 'Panner Tikka',
        category: 'Indian',
        image:'/images/panner_tikka.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 120,
    },
    {

        
        name: 'Paplet Fry',
        category: 'Sea Food',
        image:'/images/paplet.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 190,
    },
    {

        
        name: 'Pav Bhaji',
        category: 'Indian',
        image:'/images/pav_bhaji.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 180,
    },
    {

        
        name: 'Pizza Roll',
        category: 'Italian',
        image:'/images/pizza_roll.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 120,
    },
    {

        
        name: 'Pudding',
        category: 'Dessert',
        image:'/images/pudding.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 60,
    },
    {

        
        name: 'Punjabi Thali',
        category: 'Indian',
        image:'/images/punjabi_thali.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.7,
        price: 290,
    },
    {

        
        name: 'Shrimp Soup',
        category: 'Sea Food',
        image:'/images/shrimp_soup.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 270,
    },
    {

        
        name: 'Fried Shrimp',
        category: 'Sea Food',
        image:'/images/shrimp_fry.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.4,
        price: 165,
    },
    {

        
        name: 'Schezwan Fried Rice',
        category: 'Indo-Chinese',
        image:'/images/sch_fried_rice.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 190,
    },
    {

        
        name: 'Sandwich',
        category: 'Indian',
        image:'/images/sandwich.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 50,
    },
    {

        
        name: 'Samosa',
        category: 'Indian',
        image:'/images/samosa.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.2,
        price: 30,
    },
    {

        
        name: 'Roti',
        category: 'Indian',
        image:'/images/roti.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.4,
        price: 25,
    },

    {

        
        name: 'Red Sauce Pasta',
        category: 'Italian',
        image:'/images/red_sauce_pasta.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 210,
    },
    {

        
        name: 'Rava Surmai Fry',
        category: 'Sea Food',
        image:'/images/rava_surmai.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 260,
    },
    {

        
        name: 'Ramen',
        category: 'Japanese',
        image:'/images/ramen.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 400,
    },
    {

        
        name: 'Rajasthani Thali',
        category: 'Indian',
        image:'/images/rajasthani_thali.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 290,
    },
    {

        
        name: 'South Indian Thali',
        category: 'Indian',
        image:'/images/south_indian_thali.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.2,
        price: 280,
    },
    {

        
        name: 'Spicy Peanut Noodles',
        category: 'Chinese',
        image:'/images/spicy_peanut_noodles.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 300,
    },
    {
    
        
        name: 'Spicy Rice Cake',
        category: 'Korean',
        image:'/images/spicy_rice_cake.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 300,
    },
    {

        
        name: 'Spring Onion Kimchi',
        category: 'Korean',
        image:'/images/spring_onion_kimchi.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 250,
    },
    {

        
        name: 'Spring Roll',
        category: 'Indo-Chinese',
        image:'/images/spring_roll.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 70,
    },
    {

        
        name: 'Stir Fry Noodles',
        category: 'Chinese',
        image:'/images/stir_fry_noodles.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 370,
    },
    {

        
        name: 'Surmai Fry',
        category: 'Sea Food',
        image:'/images/surmai.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 180,
    },
    {

        
        name: 'Sushi',
        category: 'Japanese',
        image:'/images/sushi.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 350,
    },
    {

        
        name: 'Takoyaki',
        category: 'Japanese',
        image:'/images/takoyaki.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 470,
    },
    {

        
        name: 'Tandoori Tikka Panner',
        category: 'Indian',
        image:'/images/tandoori_tika_panner.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.6,
        price: 170,
    },
    {

        
        name: 'Veg Stir Fry Noodles',
        category: 'Indo-Chinese',
        image:'/images/veg_stir_fry_noodles.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 190,
    },
    {

        
        name: 'Waffle',
        category: 'Dessert',
        image:'/images/waffle.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 200,
    },
    {

        
        name: 'Veg Biryani',
        category: 'Indian',
        image:'/images/veg_biryani.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 230,
    },
    {

        
        name: 'Vada_pav',
        category: 'Indian',
        image:'/images/vada_pav.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.3,
        price: 20,
    },

    {

        
        name: 'Udon',
        category: 'Japanese',
        image:'/images/udon.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 300,
    },
    {

        
        name: 'Tomato Pasta',
        category: 'Italian',
        image:'/images/tomato_pasta.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.5,
        price: 290,
    },
    {

        
        name: 'Tofu Udon',
        category: 'Japanese',
        image:'/images/tofu_udon.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.9,
        price: 380,
    },
    {

        
        name: 'Tofu Stew',
        category: 'Korean',
        image:'/images/tofu_stew.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.7,
        price: 360,
    },
    {

        
        name: 'Tofu Ramen',
        category: 'Japanese',
        image:'/images/tofu_ramen.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 450,
    },
    {

        
        name: 'Tempura',
        category: 'Japanese',
        image:'/images/tempura.jpeg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.9,
        price: 400,
    },
    {
        
        
        name: 'Scallion Pancake',
        category: 'Chinese',
        image:'/images/scallion_pancakes.jpg',
        description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
        rating: 4.8,
        price: 290,
    },


];

export default dishes;
// module.exports = dishes;


// const dishes = [
//     {
//     _id:'1',
//     name: 'Aglio e Olio',
//     image: '/images/aglio_e_olio.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Italian',
//     type:'Main',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
//     },
//     {
//     _id:'2',
//     name: 'Aloo Paratha',
//     image: '/images/aloo_paratha.jpeg',
//     author: 'Jane Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 6 ,
//     prep: '30 min',
//     level: 'Medium',
//     category: 'Indian',
//     type:'Breakfast',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
//     },
//     {
//     _id:'3',
//     name: 'Fettuccine Alfredo',
//     image: '/images/alferdo_pasta.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '20 min',
//     level: 'Medium',
//     category: 'Italian',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
//     },
//     {
//     _id:'4',
//     name:'Bread',
//     image: '/images/bread.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '40 min',
//     level: 'Easy',
//     category: 'Italian',
//     type:'Main',
//     rating: 4.4,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   

//     },
//     {
//     _id:'5',
//     name: 'Bomb Cake',
//     image: '/images/bomb_cake.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '260 min',
//     level: 'Hard', 
//     category: 'Dessert',
//     type:'Dessert',
//     rating: 4.3,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'6',
//     name: 'Bibimbap',
//     image: '/images/bibimbap.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 3 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Korean',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'7',
//     name: 'Cake Shot',
//     image: '/images/cake_shot.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '50 min',
//     level: 'Medium', 
//     category: 'Dessert',
//     type:'Dessert',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
//     },
//     {
//     _id:'8',
//     name: 'Caramel Ice Cream',
//     image: '/images/caramel_icecream.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '60 min',
//     level: 'Medium', 
//     category: 'Dessert',
//     type:'Dessert',
//     rating: 4.3,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
//     },
//     {
//     _id:'9',
//     name: 'Cauliflower Chilly',
//     image: '/images/cauliflower_dry.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '75 min',
//     level: 'Medium', 
//     category: 'Indo-Chinese',
//     type:'Starter',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.'
//     },
//     {
//     _id:'10',
//     name: 'Veggies Chesse Pizza',
//     image: '/images/cheese_pizza.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 1 ,
//     prep: '90 min',
//     level: 'Easy',
//     category: 'Italian',
//     type:'Main',
//     rating: 4.0,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.'
//     },
//     {
//     _id:'11',
//     name: 'Chesse Red Sauce Pasta',
//     image: '/images/chesse_red_pasta.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '30 min',
//     level: 'Medium',
//     category: 'Italian',
//     type:'Main',
//     rating: 4.3,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
//     },
//     {
//     _id:'12',
//     name: 'Chilly Potato',
//     image: '/images/chilli_potato.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '20 min',
//     level: 'Medium',
//     category: 'Indo-Chinese',
//     type:'Starter',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.'
//     },
//     {
//     _id:'13',
//     name: 'Choco Lava Cake',
//     image: '/images/choco_lava_cake.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 1 ,
//     prep: '40 min',
//     level: 'Medium',
//     category: 'Dessert',
//     type:'Dessert',
//     rating: 4.9,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.'
//     },
//     {
//     _id:'14',
//     name: 'Churros',
//     image: '/images/churros.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '60 min',
//     level: 'Medium',
//     category: 'Dessert',
//     type:'Dessert',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.'
//     },
//     {
//     _id:'15',
//     name: 'Corn Dog', 
//     image: '/images/corn_dog.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 5 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Korean',
//     type:'Snack',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.'
//     },
//     { 
//     _id:'16',
//     name: 'Lo Mein',
//     image: '/images/chinese_noodles.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '50 min',
//     level: 'Hard',
//     category: 'Chinese',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',


//     },
//     {
//     _id:'17',
//     name: 'Clamp Pot',
//     image: '/images/clamp_pasta.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '30 min',
//     level: 'Medium',
//     category: 'Sea-Food',
//     type:'Main',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    

//     },
//     {
//     _id:'18',
//     name: 'Crab Curry',
//     image: '/images/crab.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '50 min',
//     level: 'Hard',
//     category: 'Sea-Food',
//     type:'Main',
//     rating: 4.9,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
//     },
//     {
//     _id:'19',
//     name: 'Dahi Vada',
//     image: '/images/dahi_vada.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Indian',
//     type:'Breakfast/Snack',
//     rating: 3.9,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'20',
//     name: 'Masala Dosa',
//     image: '/images/dosa.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '30 min',
//     level: 'Medium',
//     category: 'Indian',
//     type:'Breakfast',
//     rating: 4.4,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
     
//     },
//     {
//     _id:'21',
//     name: 'Dhokla',
//     image: '/images/dhokla.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '60 min',
//     level: 'Medium',
//     category: 'Indian',
//     type:'Breakfast/Snack',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
//     },
//     {
//     _id:'22',
//     name: 'Fried Milk',
//     image: '/images/fried_milk.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 5 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Dessert',
//     type:'Dessert',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    

//     },
//     {
//     _id:'23',
//     name: 'Dumpling',
//     image: '/images/dumpling.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '25 min',
//     level: 'Easy',
//     category: 'Chinese',
//     type:'Side-Dish',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'24',
//     name: 'Gralic Bread',
//     image: '/images/garlic_bread.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Italian',
//     type:'Main',
//     rating: 4.0,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   

//     },
//     {
//     _id:'25',
//     name: 'Garlic Naan',
//     image: '/images/garlic_naan.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'26',
//     name: 'Garlic Sesame Noodles',
//     image: '/images/garlic_sesame_noodles.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '25 min',
//     level: 'Medium',
//     category: 'Korean',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'27',
//     name: 'Gujrati Thali',
//     image: '/images/gujrati_thali.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '300 min',
//     level: 'Hard',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
//     },
//     {
//     _id:'28',
//     name: 'Gimbap',
//     image: '/images/gimbap.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Korean',
//     type:'Side-Dish',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    

//     },
//     {
//     _id:'29',
//     name: 'Japense Curry',
//     image: '/images/jap_curry.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '80 min',
//     level: 'Medium',
//     category: 'Japanese',
//     type:'Main',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    


//     },
//     {
//     _id:'30',
//     name: 'Mutter Puloa and Kadai Panner',
//     image: '/images/kadai_pan_puloa_combo.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '150 min',
//     level: 'Hard',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
      
//     },
//     {
//     _id:'31',
//     name: 'Kimchi',
//     image: '/images/kimchi.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 10 ,
//     prep: '15 min',
//     level: 'Easy',
//     category: 'Korean',
//     type:'Side-Dish',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
    
//     },
//     {
//     _id:'32',
//     name: 'Lobster',
//     image: '/images/lobster.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '50 min',
//     level: 'Medium',
//     category: 'Sea-Food',
//     type:'Main',
//     rating: 4.9,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
    
//     },
//     {
//     _id:'33',
//     name: 'Kimchi Noodles',
//     image: '/images/kimchi_noodles.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '15 min',
//     level: 'Easy',
//     category: 'Korean',
//     type:'Main',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
   
//     },
//     {
//     _id:'34',
//     name: 'Maharashtrian Thali',
//     image: '/images/maharashtrian_thali.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '200 min',
//     level: 'Hard',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
   
//     },
//     {
//     _id:'35',
//     name: 'Macroon',
//     image: '/images/macrooon.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 5 ,
//     prep: '50 min',
//     level: 'Medium',
//     category: 'Dessert',
//     type:'Dessert',
//     rating: 4.2,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
   
//     },
//     {
//     _id:'36',
//     name: 'Manchurian',
//     image: '/images/manchurian.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 6 ,
//     prep: '20 min',
//     level: 'Medium',
//     category: 'Indo-Chinese',
//     type:'Snack',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'37',
//     name: 'Misal Pav',
//     image: '/images/misal_pav.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 5 ,
//     prep: '30 min',
//     level: 'Medium',
//     category: 'Indian',
//     type:'Breakfast',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',

//     },
//     {
//     _id:'38',
//     name: 'North Indian Thali',
//     image: '/images/north_indian_combo.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '110 min',
//     level: 'Hard',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
     
//     },
//     {
//     _id:'39',
//     name: 'Naan',
//     image: '/images/naan.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 5 ,
//     prep: '35 min',
//     level: 'Easy',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
     
//     },
//     {
//     _id:'40',
//     name: 'Omu Rice',
//     image: '/images/omu_rice.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '40 min',
//     level: 'Medium',
//     category: 'Japanese',
//     type:'Main',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
     
//     },
//     {
//     _id:'41',
//     name: 'Pav Bhaji',
//     image: '/images/pav_bhaji.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '40 min',
//     level: 'Medium',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
     
//     },
//     {
//     _id:'42',
//     name: 'Panner Tikka',
//     image: '/images/panner_tikka.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '30 min',
//     level: 'Medium',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',

//     },
//     {
//     _id:'43',
//     name: 'Paplet Fry',
//     image: '/images/paplet.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '15 min',
//     level: 'Easy',
//     category: 'Sea Food',
//     type:'Main',
//     rating: 4.4,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    

//     },
//     {
//     _id:'44',
//     name: 'Ramen',
//     image: '/images/ramen.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '100 min',
//     level: 'Hard',
//     category: 'Japanese',
//     type:'Main',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'45',
//     name: 'Spicy Peanut Noodles',
//     image: '/images/spicy_peanut_noodles.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '20 min',
//     level: 'Medium',
//     category: 'Chinese',
//     type:'Main',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'46',
//     name: 'Spicy Rice Cake',
//     image: '/images/spicy_rice_cake.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Korean',
//     type:'Side-Dish/Snack',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    

//     },
//     {
//     _id:'47',
//     name: 'Stir Fry Noodles',
//     image: '/images/stir_fry_noodles.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '25 min',
//     level: 'Medium',
//     category: 'Chinese',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',

//     },
//     {
//     _id:'48',
//     name: 'Surmai Fry',
//     image: '/images/surmai.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '10 min',
//     level: 'Easy',
//     category: 'Sea-Food',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',


//     },
//     {
//     _id:'49',
//     name: 'Tandoori Tikka Panner',
//     image: '/images/tandoori_tika_panner.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '30 min',
//     level: 'Medium',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    

//     },
//     {
//     _id:'50',
//     name: 'Waffle',
//     image: '/images/waffle.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Dessert',
//     type:'Dessert',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
     
//     },
//     {
//     _id:'51',
//     name: 'Veg Stir Fry Noodles',
//     image: '/images/veg_stir_fry_noodles.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '36 min',
//     level: 'Easy',
//     category: 'Indo-Chinese',
//     type:'Main',
//     rating: 4.3,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   
   
//     },
//     {
//     _id:'52',
//     name: 'Veg Biryani',
//     image: '/images/veg_biryani.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 5 ,
//     prep: '90 min',
//     level: 'Hard',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
   
//     },
//     {
//     _id:'53',
//     name: 'Vada_pav',
//     image: '/images/vada_pav.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 6 ,
//     prep: '30 min',
//     level: 'Easy',
//     category: 'Indian',
//     type:'Snack',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'54',
//     name: 'Tomato Pasta',
//     image: '/images/tomato_pasta.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '25 min',
//     level: 'Easy',
//     category: 'Italian',
//     type:'Main',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'55',
//     name: 'Tofu Stew',
//     image: '/images/tofu_stew.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '40 min',
//     level: 'Medium',
//     category: 'Korean',
//     type:'Main',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     { 
//     _id:'56',
//     name: 'Tempura',
//     image: '/images/tempura.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Japanese',
//     type:'Side-Dish',
//     rating: 4.9,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'57',
//     name: 'Scallion Pancake',
//     image: '/images/scallion_pancakes.jpg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '10 min',
//     level: 'Easy',
//     category: 'Chinese',
//     type:'Side-Dish',
//     rating: 4.3,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   
//     },
//     {
//     _id:'58',
//     name: 'Tofu Ramen',
//     image: '/images/tofu_ramen.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '100 min',
//     level: 'Easy',
//     category: 'Japanese',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   
//     },
//     {
//     _id:'59',
//     name: 'Tofu Udon',
//     image: '/images/tofu_udon.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '35 min',
//     level: 'Hard',
//     category: 'Japanese',
//     type:'Main',
//     rating: 4.9,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   
//     },
//     {
//     _id:'60',
//     name: 'Takoyaki',
//     image: '/images/takoyaki.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '20 min',
//     level: 'Medium',
//     category: 'Japanese',
//     type:'Snack',
//     rating: 4.9,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'61',
//     name: 'Spring Roll',
//     image: '/images/spring_roll.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '25 min',
//     level: 'Easy',
//     category: 'Indo-Chinese',
//     type:'Starter/Snack',
//     rating: 4.2,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     { 
//     _id:'62',
//     name:'Rajasthani Thali',
//     image: '/images/rajasthani_thali.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '180 min',
//     level: 'Hard',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   

//     },
//     {
//     _id:'63',
//     name:'Samosa',
//     image: '/images/samosa.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '25 min',
//     level: 'Easy',
//     category: 'Indian',
//     type:'Snack',
//     rating: 4.2,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    

//     },
//     {
//     _id:'64',
//     name:'Schezwan Fried Rice',
//     image: '/images/sch_fried_rice.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 5 ,
//     prep: '15 min',
//     level: 'Medium',
//     category: 'Indo-Chinese',
//     type:'Main',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   

//     },
//     {
//     _id:'65',
//     name:'Shrimp Soup',
//     image: '/images/shrimp_soup.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 5 ,
//     prep: '20 min',
//     level: 'Medium',
//     category: 'Sea-Food',
//     type:'Main',
//     rating: 4.9,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    

//     },
//     {
//     _id:'66',
//     name: 'Fried Shrimp',
//     image: '/images/shrimp_fry.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '10 min',
//     level: 'Easy',
//     category: 'Sea-Food',
//     type:'Main',
//     rating: 4.6,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   

//     },
//     {
//     _id:'67',
//     name: 'Punjabi Thali',
//     image: '/images/punjabi_thali.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '150 min',
//     level: 'Hard',
//     category: 'Indian',
//     type:'Main',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
     
//     },
//     {
//     _id:'68',
//     name: 'Pudding',
//     image: '/images/pudding.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Dessert',
//     type:'Dessert',
//     rating: 4.3,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
  
//     },
//     {
//     _id:'69',
//     name: 'Panner Paratha',
//     image: '/images/panner_paratha.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Indian',
//     type:'Breakfast/Snack',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',

//     },
//     {
//     _id:'70',
//     name: 'Palak Paratha',
//     image: '/images/palak_paratha.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Indian',
//     type:'Breakfast/Snack',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'71',
//     name: 'Onigri',
//     image: '/images/onigiri.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '10 min',
//     level: 'Easy',
//     category: 'Japanese',
//     type:'Snack',
//     rating: 4.7,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    

//     },
//     {
//     _id:'72',
//     name: 'Mushroom Noodles',
//     image: '/images/mush_noodles.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '20 min',
//     level: 'Medium',
//     category: 'Chinese',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'73',
//     name: 'Tandoori Fried Momos',
//     image: '/images/momo.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '25 min',
//     level: 'Medium',
//     category: 'Indo-Chinese',
//     type:'Snack',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
    

//     },
//     {
//     _id:'74',
//     name: 'Okonomiyaki',
//     image: '/images/okonomiyaki.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 4 ,
//     prep: '20 min',
//     level: 'Medium',
//     category: 'Japanese',
//     type:'Snack',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'75',
//     name: 'Mochi',
//     image: '/images/mochi.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 5 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Japnese',
//     type:'Dessert',
//     rating: 4.9,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'76',
//     name: 'Medu Vada',
//     image: '/images/medu_vada.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 8 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Indian',
//     type:'Breakfast',
//     rating: 4.5,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   
//     },
//     {
//     _id:'77',
//     name: 'Jajangmyeon',
//     image: '/images/jajangmeyon.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '35 min',
//     level: 'Hard',
//     category: 'Korean',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
    
//     },
//     {
//     _id:'78',
//     name: 'Garlic Parsley Pasta',
//     image: '/images/garlic_parsley_pasta.jpeg',
//     author: 'John Doe',
//     description: 'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
//     ingredients: 'Lorem ipsum dolor, consectetur adipiscing elit, sed do eiusmod tempor, incididunt, ed do eiusmod , et dolore magna, Ut varius ex, ut purus porttitor, a facilisis orci, condimentum, Nullam in elit et, sapien, ornare pellentesque, at ac lorem',
//     direction: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//     serves: 2 ,
//     prep: '20 min',
//     level: 'Easy',
//     category: 'Italian',
//     type:'Main',
//     rating: 4.8,
//     note:'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi.',
   
//     },

// ];

// export default dishes;
// // module.exports = dishes;
