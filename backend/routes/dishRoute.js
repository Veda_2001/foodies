import express from 'express';
import { getAllDishes, getCategory, getDishById} from '../controllers/dishController.js';

const router = express.Router();

router.route('/').get(getAllDishes);
router.route('/:category').get(getCategory)
router.route('/dish/:id').get(getDishById);

export default router;