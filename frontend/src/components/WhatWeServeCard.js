import { Flex, Link, Icon } from "@chakra-ui/react";
import {Link as RouterLink} from 'react-router-dom';


const WhatWeServe = ({label,url}) => {
    return (
        <Flex
            p='3'
            border='1px solid gray'
            _hover={{shadow:'2xl'}}
        >
            <Link
                as={RouterLink}
                to={url}
                textTransform='uppercase'
                fontSize='md'
                fontWeight='bold'
                p='3'
                letterSpacing= 'wide'
                border='1px solid gray'
                _hover={{textDecor:'none', bgColor:'gray', color:'white'}}
            >
                    {label}
            </Link>
        </Flex>
    )

};

export default WhatWeServe;