import { Flex, Box, Heading, Text, Icon } from "@chakra-ui/react";
import {AiTwotoneStar} from 'react-icons/ai';

const HomeScreenRev = ({name,email,rev,rating}) => {
    return (
        <Box borderRadius='md' bgColor='gray.100' _hover={{ shadow: 'md' }}>
            <Flex py='5' px='4' direction='column' justifyContent='space-between'>
                    <Flex
                        direction='row'
                        justifyContent='space-between'
                    >
                        <Heading as='h4' fontSize='lg' mb='2'>
                            {name}
                        </Heading>
                        <Flex
                            p='2'
                            bgColor='gray'
                            alignItems='center'
                            borderRadius='10'
                        >
                            <Icon as={AiTwotoneStar} mr='2' color='yellow' />
                            {rating}
                        </Flex>
                       
                        </Flex>
                    <Text fontSize='md' fontWeight='bold' mb='3'>
						{email}
					</Text>
                                        
					<Flex alignItems='center' mt='5' justifyContent='center'>
						<Text fontSize='sm'>
							{rev}
						</Text>
					</Flex>
				</Flex>
        </Box>
    )
};

export default HomeScreenRev;