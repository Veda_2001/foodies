import { Box,Grid, Flex, Heading, Image, Link, Text, Icon } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';
import Rating from './Rating';


const DishCard = ({dish}) => {
    return(
        <Link
			as={RouterLink}
			to={`/menu/dish/${dish._id}`}
			_hover={{ textDecor: 'none' }}>
			<Box 
            p='5'
            align='center' 
            bgColor='white' 
            _hover={{ shadow: 'lg' }} 
        >
				<Image
					src={dish.image}
					alt={dish.name}
					w='350'
					h='300'
					objectFit='cover'
				/>
				<Flex py='2' px='4' direction='column' justifyContent='center'>
					<Heading as='h4' fontSize='lg' mb='3'>
						{dish.name}
					</Heading>
					<Flex alignItems='center' justifyContent='space-evenly'>
						<Rating color='yellow.500' value={dish.rating} />
						<Text fontSize='md' fontWeight='bold' color='blue.600'>
							₹{dish.price}
						</Text>
					</Flex>
				</Flex>
                <Flex
                  alignItems='center' 
                  justifyContent='space-evenly'  
                >
                    <Link
                        as={RouterLink}
                        to='/'
                        p='2'
                        color='white'
                        bgColor='facebook.300'
                        borderRadius='10'
                        _hover={{textDecor:'none'}}
                        // onClick={toOrderPage}
                    >
                        Buy Now
                    </Link>
                </Flex>
			</Box>
		</Link>
    )
};

export default DishCard;