import { Flex, Image , Heading , Text, Icon, Link, Button} from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';
import {GrDeliver, GrLocation} from 'react-icons/gr';
import {MdPayment, MdStars} from 'react-icons/md';
import FeatureCard from '../components/FeatureCard';

const WhyUsCard = () => {

    const toReviewPg = {

    }
    
    return (
        <Flex
                direction='column'
                p='20'
                gap='3'
                align='center'
            >  
                <Heading
                    as='h3'
                    fontWeight='bold'
                    fontFamily='serif'
                    letterSpacing='wide'
                >
                    Why Choose <span style={{color: 'red'}} >Us</span> ?
                </Heading>

                <Image src='/images/Bishoku_logo.png' alt='bishokuka' 
                objectFit='cover'
                h='80' w='80'/>

                <Text
                    textAlign='center'
                    px='10'
                >
                    Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam 
                    in elit et sapien ornare pellentesque at ac lorem.
                </Text>

                <Flex
                    direction={{base:'column', md:'row'}}
                    gap='3'
                    align='center'
                    px='20'
                >
                    <FeatureCard 
                    icon= {<Icon as={MdStars}  h='6' w='6'/>}
                    label='Top Quality'
                    text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                    />
                    <FeatureCard 
                    icon={<Icon as={MdPayment}  h='6' w='6'/>}
                    label='Easy Payment'
                    text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                    />
                    <FeatureCard 
                    icon={<Icon as={GrDeliver}  h='6' w='6'/>}
                    label='Free Delivery'
                    text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                    />
                    <FeatureCard 
                    icon={<Icon as={GrLocation}  h='6' w='6'/>}
                    label='Easy Order'
                    text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                    />      
                </Flex>
                
                <Link
                            as={RouterLink}
                            to='/about'
                            p='2'
                            bgColor='telegram.700'
                            borderRadius='10'
                            color= 'white'
                            _hover={{textDecor:'none'}}
                        >
                            Learn More
                        </Link>
            </Flex> 
    )
};

export default WhyUsCard;