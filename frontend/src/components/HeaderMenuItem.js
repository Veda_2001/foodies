import { Link } from "@chakra-ui/react";
import { Link as RouterLink } from 'react-router-dom';

const HeaderMenuItem = ({url,label}) => {
    return(
        <Link
                as={RouterLink}
                to={url}
                fontSize='sm'
                letterSpacing='wide'
                textTransform='uppercase'
                fontWeight='bold'
                borderRadius='10'
                p='2'
                display='flex'
                alignItems='center'
                mb={{base:'2',md:'0'}}
                _hover={{textDecor: 'none', color: 'white ', bgColor:'facebook.500'}}>
                {label}
        </Link>
    );
}
export default HeaderMenuItem;