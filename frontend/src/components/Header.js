import { Flex, Icon, Box, Link, Heading, Image,Button,
	Menu,
	MenuButton,
	MenuItem,
	MenuList,
} from "@chakra-ui/react";
import React from "react";
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { IoChevronDown } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import {FaRegUserCircle, FaShoppingCart} from 'react-icons/fa';
import {MdMenuOpen} from 'react-icons/md';
import { useState} from 'react';
import {logout} from '../actions/userAction';
import HeaderMenuItem from "./HeaderMenuItem";

const Header = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const [show, setShow] = useState(false);

	const userLogin = useSelector((state) => state.userLogin);
	const { userInfo } = userLogin;

	const logoutHandler = () => {
		dispatch(logout());
		navigate('/login');
	};

return (
<Flex
   as='header'
   align='center'
   justifyContent='space-between'
   shadow='md'
   bgColor='white'
   wrap='wrap'
   p='6'
   px='6'
   w='100%'
   pos='fixed'
   top='0'
   left='0'
>
   <Flex
	   align='center'
	   direction='column'
	   wrap="wrap"
   >
	   <Box>
	   <Image src='/images/Bishoku_logo.png' alt='bishokuka' h='50' objectFit='cover'/>
	   </Box>
   <Heading
	   as='h1'
	   fontWeight='bold'
	   size='md'
	   letterSpacing='wide' 
	   fontFamily='cursive'           
   >
	   Bishoku-Ka
   </Heading>
   </Flex>
   
		  {/* */}

   <Flex
	   align='center'
	   justifyContent='space-between'
	   gap='3'
	   wrap='wrap'
	   mr='8'
   >
	   <HeaderMenuItem
		   url='/'
		   label='HOME'
	   />
	   <HeaderMenuItem
		   url='/menu'
		   label='MENU'
	   />
	   <HeaderMenuItem
		   url='/review'
		   label='REVIEW'
	   />
	   <HeaderMenuItem
		   url='/about'
		   label='ABOUT US'
	   />
   </Flex>

   <Box
	   display={{ base: 'block', md: 'none' }}
	   onClick={() => setShow(!show)}>
	   <Icon as={MdMenuOpen} color='black' w='7' h='7' />
   </Box>

   <Box
		alignItems='center'
	   display={{ base: show ? 'block' : 'none', md: 'flex' }}
	   width={{ base: 'full', md: 'auto' }}
	   mt={{ base: '3', md: '0' }}>

   
		   {/* Login */}
		   {userInfo ? (
					<Menu>
						<MenuButton
							as={Button}
							rightIcon={<IoChevronDown />}
							mr='5'
							_hover={{ textDecor: 'none', opacity: '0.7' }}>
							{userInfo.name}
						</MenuButton>
						<MenuList>
							<MenuItem as={RouterLink} to='/profile'>
								Profile
							</MenuItem>
							<MenuItem onClick={logoutHandler}>Logout</MenuItem>
						</MenuList>
					</Menu>
				) : (
	   <Link
		   as={RouterLink}
		   to='/login'
	   >
		   <Icon as={FaRegUserCircle } 
		   mb={{base:'3',md:'0'}}
		   _hover={{textDecor: 'none', color: 'gray.500'}}
		   mr='5' w='5' h='5'/>
	   </Link>
	   )}
		  

		   {/* Cart */}
	   <Link
		   as={RouterLink}
		   to='/cart'
	   >
		   <Icon as={FaShoppingCart } 
		   mb={{base:'3',md:'0'}}
		   _hover={{textDecor: 'none', color: 'gray.500'}}
		   mr='5' w='5' h='5'/>
	   </Link>
	   
	   
   </Box>
   

</Flex>

)
};

export default Header;
