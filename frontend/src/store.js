import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { dishCategoryReducer, dishDetailsReducer } from './reducers/dishReducer';
import {cartReducer} from './reducers/cartReducer';
import { userLoginReducer, userRegisterReducer,userDetailsReducer, userUpdateProfileReducer} from './reducers/userReducer';
import { orderCreateReducer, orderDetailsReducer, orderPayReducer, } from './reducers/orderReducer';

const reducer = combineReducers({
	dishCategory: dishCategoryReducer,
	dishDetails: dishDetailsReducer,
	cart: cartReducer,
	userLogin: userLoginReducer,
	userRegister: userRegisterReducer,
	userDetails: userDetailsReducer,
	userUpdateProfile: userUpdateProfileReducer,
	orderCreate: orderCreateReducer,
	orderDetails: orderDetailsReducer,
	orderPay: orderPayReducer,
});


const cartItemsFromStorage = localStorage.getItem('cartItems')
	? JSON.parse(localStorage.getItem('cartItems'))
	: [];

	const userInfoFromStorage = localStorage.getItem('userInfo')
	? JSON.parse(localStorage.getItem('userInfo'))
	: null;

	const deliveryAddressFromStorage = localStorage.getItem('deliveryAdress')
	? JSON.parse(localStorage.getItem('deliveryAdress'))
	: {};
		
	const paymentMethodFromStorage = localStorage.getItem('paymentMethod')
	? JSON.parse(localStorage.getItem('paymentMethod'))
	: 'paypal';

const initialState = {
	cart: {
		cartItems: cartItemsFromStorage,
		deliveryAddress: deliveryAddressFromStorage,
		paymentMethod: paymentMethodFromStorage,
	},
	userLogin: { userInfo: userInfoFromStorage },
};

const middlewares = [thunk];

const store = createStore(
	reducer,
	initialState,
	composeWithDevTools(applyMiddleware(...middlewares))
);

export default store;
