import { Flex } from "@chakra-ui/react";
import { BrowserRouter, Route, Routes   } from "react-router-dom";
import Footer from "./components/Footer";
import Header from "./components/Header";
import HomeScreen from "./screens/HomeScreen";
import MenuScreen from "./screens/MenuScreen";
import DishScreen from "./screens/DishScreen";
import AboutScreen from "./screens/AboutScreen";
import CartScreen from "./screens/CartScreen";
import LogScreens from "./screens/LoginScreens";
import RegisterScreen from "./screens/RegisterScreens";
import ProfileScreen from "./screens/UProfileScreen";
import DeliveryScreen from "./screens/DeliveryScreen";
import PaymentScreen from "./screens/PaymentScreen";
import OrderScreen from "./screens/OrderScreen";
import FoodOrderScreen from "./screens/FoodOrderScreen";




const App = () => {
  return (
    <BrowserRouter>
      <Header/>
      <Flex
        as='main'
        mt='139px'
				direction='column'
				py='6'
				px='6'
      >
        <Routes>
					<Route path='/' element={<HomeScreen/>} />
          <Route path='/menu' element={<MenuScreen/>} />
          <Route path='/menu/dish/:id' element={<DishScreen/>} />
          <Route path='/about' element={<AboutScreen/>} />
          <Route path='/cart' element={<CartScreen/>} />
          <Route path='/cart/:id' element={<CartScreen />} />
          <Route path='/login' element={<LogScreens />} />
          <Route path='/register' element={<RegisterScreen />} />
          <Route path='/profile' element={<ProfileScreen />} />
          <Route path='/delivery' element={<DeliveryScreen />} />
          <Route path='/payment' element={<PaymentScreen />} />
          <Route path='/forder' element={<OrderScreen />} />
          <Route path='/order/:id' element={<FoodOrderScreen />} />

				</Routes>
      
      
      </Flex>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;
