import axios from 'axios';

import {
    DISH_CATEGORY_REQUEST,
    DISH_CATEGORY_SUCCESS,
    DISH_CATEGORY_FAIL,
    DISH_DETAILS_REQUEST,
    DISH_DETAILS_SUCCESS,
    DISH_DETAILS_FAIL,

} from '../constants/dishConstant';

export const categoryDish = (category) => async (dispatch) => {
    try{
        dispatch({ type: DISH_CATEGORY_REQUEST });

        const { data } = await axios.get(`/api/menu/${category}`);

        dispatch({ type: DISH_CATEGORY_SUCCESS, payload: data });
    } catch (err) {
        dispatch({
            type: DISH_CATEGORY_FAIL,
            payload:
                err.response && err.response.data.message
                ? err.response.data.message
                : err.message,
        });
    }
};



export const detailsDish = (id) => async (dispatch) => {
    try{
        dispatch({ type: DISH_DETAILS_REQUEST });

        const { data } = await axios.get(`/api/menu/dish/${id}`);

        dispatch({ type: DISH_DETAILS_SUCCESS, payload: data });
    } catch (err) {
        dispatch({
            type: DISH_DETAILS_FAIL,
            payload:
                err.response && err.response.data.message
                ? err.response.data.message
                : err.message,
        });
    }
};

