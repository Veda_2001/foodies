import {
	Button,
	Flex,
	FormControl,
	FormLabel,
	Heading,
	Input,
	Image,
	Spacer,
} from '@chakra-ui/react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { saveDeliveryAddress } from '../actions/cartAction';
import CheckoutSteps from '../components/CheckoutSteps';
import FormContainer from '../components/FormContainer';

const DeliveryScreen = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const cart = useSelector((state) => state.cart);
	const { deliveryAddress } = cart;

	const [address, setAddress] = useState(deliveryAddress.address || '');
	const [city, setCity] = useState(deliveryAddress.city || '');
	const [postalCode, setPostalCode] = useState(
		deliveryAddress.postalCode || ''
	);

	const submitHandler = (e) => {
		e.preventDefault();
		dispatch(saveDeliveryAddress({ address, city, postalCode }));
		navigate('/payment');
	};

	return (
		<Flex w='full' alignItems='center' justifyContent='center' py='5'>
            <Image src='/images/delivery.png' alt='delivery' objectFit='cover' />
			<FormContainer>
				<Heading as='h2' mb='8' fontSize='3xl'>
					Delivery
				</Heading>
				<CheckoutSteps step1 step2 />

				<form onSubmit={submitHandler}>
					{/* Address */}
					<FormControl id='address'>
						<FormLabel htmlFor='address'>Address</FormLabel>
						<Input
							id='address'
							type='text'
							placeholder='Your Address'
							value={address}
							onChange={(e) => setAddress(e.target.value)}
						/>
					</FormControl>

					<Spacer h='3' />

					{/* City */}
					<FormControl id='city'>
						<FormLabel htmlFor='city'>City</FormLabel>
						<Input
							id='city'
							type='text'
							placeholder='Your City'
							value={city}
							onChange={(e) => setCity(e.target.value)}
						/>
					</FormControl>

					<Spacer h='3' />

					{/* Postal Code */}
					<FormControl id='postalCode'>
						<FormLabel htmlFor='postalCode'>Postal Code</FormLabel>
						<Input
							id='postalCode'
							type='text'
							placeholder='Your Postal Code'
							value={postalCode}
							onChange={(e) => setPostalCode(e.target.value)}
						/>
					</FormControl>

					<Spacer h='3' />

					<Button type='submit' colorScheme='facebook' mt='4'>
						Continue
					</Button>
				</form>
			</FormContainer>
		</Flex>
	);
};

export default DeliveryScreen;
